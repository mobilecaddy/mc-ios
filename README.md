# README #

MobileCaddy Device Builds for Salesforce Mobile SDK 3v1.

## How do I get set up? ###

### Install the dependencies

`npm install -g cordova ionic gulp`

### Set up the project

* Download/clone the repo
* cd into it
* run `npm install` (sudo if on a mac)
* run `bower install` 
* run `gulp mc-setup`
* run `sass --watch  scss/:www/css/` - Let this for a few seconds until you see the message *Listen will be polling...*, then you can *Ctrl+c* to stop it, the work we needed has been completed.
* Set the following relevant values in your *config.xml* file
    * *Widget ID* (to be something like net.mobilecaddy.biz001)
    * *Name*
    * *Description*
* Set your consumer key in the *www/bootconfig.json* file.

###  Build the project

```
ionic platform add ios
ionic build ios
```

At this point the project can now be opened in xcode if you want. Got to *platforms/ios* and you should see a project file. Or you can run it in the ios simulator from the command line;

```
ionic emulate ios
```

## Icons and Splash Screen

We use the brilliant Ionic tools to make light work of this. Put your png file into the *resources/ios* dir as icon.png and splash.png and then run the following, it will create all the image assets you'll need. For more info see [this page](http://ionicframework.com/blog/automating-icons-and-splash-screens/)
```
ionic resources
```

## Known issues

* You may find that your app hangs at the initial loading screen. In this instance check to see of *deviceReady* has fired. If not (which is kinda likely in first run up) then run the following. This is to work-around a somewhat intermitent cordova bug;
```
cordova plugin remove org.apache.cordova.network-information
cordova plugin add org.apache.cordova.network-information
```
