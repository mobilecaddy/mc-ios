var SupportMc = {};

SupportMc.constants = {
		buildVersion : '001',
		buildName : 'BIZ001',
		bootstrapPage : '/apex/mc_package_002__MobileCaddyBootstrap001_mc',
		offerLanguageChoice : true
} // end constants

SupportMc.variables = {
		creds : null,
		chosenLanguage : ''
} // end variables

SupportMc.singleton = (function() {

	// Called when the device ready event fires
	function onDeviceReady() {
		console.log("in onDeviceReady (1)");
		var smartstore = cordova.require("com.salesforce.plugin.smartstore");
		logMessage('In onDeviceReady');

		// Prevent user using the back butt-on
		document.addEventListener("backbutton", function() {}, true);

		// Check the status of the build
		getExistingBuildStatus(
				function(status) {
					logMessage('Returned status = ' + status);
					if (status == 'Complete') {

						// We already have a complete build so redirect to the url
						// from the app soup
						var urlQuerySpec = smartstore.buildExactQuerySpec("Name", 'startPageURL', 50);
						querySoupRecordsWithQuerySpec(
								'appSoup',
								urlQuerySpec,
								function(urlRecs) {

									// Check we got back a single row
									if (urlRecs.length != 1) throw "expected 1 startPageURL entry in app soup, received " + urlRecs.length;
									logMessage('Already installed, redirecting to ' + urlRecs[0].CurrentValue);
									window.location.href = urlRecs[0].CurrentValue;
								},
								function(e) {
									logMessage('Failed to read url from app soup with error ' + e);
								});

					} // end status Complete check if
					else {

						// None means first time through so continue
						// with the language choice
						if (status == 'None') {
							SupportMc.singleton.startUp();

						} // end None check if
						else {

							// Must be error condition so logout.
							// This will remove the soups
							alert('Build Corrupted.  Please reinstall');
						} // end status None check else
					} // end status Complete check else
				}, // end success callback
				function(e) {
					logMessage('Error checking build status = ' + e);
				});
	} // end onDeviceReady


	function getOrientation() {
		if (Math.abs(window.orientation) === 90) {
	        return 'landscape';
	    } else {
	    	return 'portrait';
	    }
	} // end getOrientation

	// Output contents of an object
	// This determines all keys within the object
	function displayObjectDetails(obj) {
		for (var fieldDef in obj) {
			logMessage(fieldDef + ' has value = ' + obj[fieldDef]);
		} // end loop through the object fields
	} // end displayObjectDetails

	// Log the error
	function logMessage(message) {
		console.log(message);
	} // end logMessage

	// Generic error message - put on error callbacks
	function error(e) {
		logMessage('Error found = ' + e);
	} // end error function

	// Calls the success function with the set of records
	function collectRecsFromCursor(cursor,success,error) {
		var smartstore = cordova.require("com.salesforce.plugin.smartstore");
	    var entries = [];

	    function addEntriesFromCursor(cursor) {
			$j.each(cursor.currentPageOrderedEntries, function(i, entry) {
			    entries.push(entry);
			});
	        if(cursor.currentPageIndex < cursor.totalPages - 1) {
	            smartstore.moveCursorToNextPage(cursor, addEntriesFromCursor);
	        }
	        else {
	            smartstore.closeCursor(cursor, onCursorClosed);
	        }
	    }

	    function onCursorClosed(cursor) {
	        success(entries);
	    }

	    // Let's start reading from cursor
	    addEntriesFromCursor(cursor);
	} // end collectRecsFromCursor

	function querySoupRecordsWithQuerySpec(soupName,querySpec,success,error) {
		var smartstore = cordova.require("com.salesforce.plugin.smartstore");
		smartstore.querySoup(	soupName,
								querySpec,
								function(cursor) {
									collectRecsFromCursor(	cursor,
														function(records) {
															success(records);
														},
														error);
								},
								error);
	} // end querySoupRecordsWithQuerySpec

	// Return the existing build status:
	// Complete : built and running
	// Partial : not good - build must have failed
	// None : not yet built, ie initial instal

	function getExistingBuildStatus(success,error) {
		logMessage('In getExistingBuildStatus');
		var smartstore = cordova.require("com.salesforce.plugin.smartstore");
		smartstore.soupExists(
				'appSoup',
				function(exists) {

					// If it exists, see if we have a success build status.  If so
					// redirect to the app
					if (exists) {
						logMessage('device: app soup exists');

						// Get back the build status.  If it exists and is 'success' then redirect to the cached app
						// Otherwise they have failed to build and need to reinstall the app
						var querySpec = smartstore.buildExactQuerySpec("Name", 'buildStatus', 50);
						querySoupRecordsWithQuerySpec(
								'appSoup',
								querySpec,
								function(appSoupRecs) {

									// Check we got back a single row
									if (appSoupRecs.length == 1) {

										logMessage('device: build status = ' + appSoupRecs[0].CurrentValue);

										// If success then build has been done successfully already so crack on
										if (appSoupRecs[0].CurrentValue == 'Complete') {
											success('Complete');
										}
										else {
											success('Partial');
										}
									} // end record in app soup check if
									else {

										// App soup exists but no row or multiple rows! Should not be possible as the
										// row is written on creation
										success('Failed');
									} // end record in app soup check else
								}, // end app soup callback
								error);
					} // app soup exists check if
					else {

						// App soup not yet built
						success('None');
					} // app soup exists check else
				}, // end exists callback
				error);
	} // end getExistingBuildStatus

	return {
		addEventListeners : function() {
			console.log('Adding event listeners, deviceready');
		    document.addEventListener("deviceready", onDeviceReady, false);
		}, // end addEventListeners

		// Application startup
		startUp : function() {
			var smartstore = cordova.require("com.salesforce.plugin.smartstore");
			var bootstrap = cordova.require("com.salesforce.util.bootstrap");
			var oauth = cordova.require("com.salesforce.plugin.oauth");

			// Continue build only if the device is online
			if (bootstrap.deviceIsOnline()) {

				logMessage('device is online');

				// Attempt an authenticate
				oauth.authenticate(

					// Authenticate success callback
					function(creds) {

						// Store credentials - contains user id, urls etc
						SupportMc.variables.creds = creds;

						// Create soup structure
						var appSoupStructure = [
						    {path:"Name",type:"string"},
							{path:"CurrentValue",type:"string"},
						    {path:"NewValue",type:"string"}
						];

						// Register the soup
						smartstore.registerSoup(
								'appSoup',
								appSoupStructure,
				                function() {
									console.log('appSoup registered');
									// Soup registered so populate it with the build version
									var upsertList = [];
									upsertList.push({Name : 'buildName', CurrentValue : SupportMc.constants.buildName, NewValue : SupportMc.constants.buildName});
                                    upsertList.push({Name : 'buildOS', CurrentValue : device.platform, NewValue : device.platform});
									upsertList.push({Name : 'buildVersion', CurrentValue : SupportMc.constants.buildVersion, NewValue : SupportMc.constants.buildVersion});
									upsertList.push({Name : 'deviceUuid', CurrentValue : device.uuid, NewValue : device.uuid});
									//upsertList.push({Name : 'deviceUuid', CurrentValue : 'c86d3e94574debug', NewValue : 'c86d3e94574debug'});
									upsertList.push({Name : 'buildStatus', CurrentValue : 'Initial', NewValue : 'Initial'});

									// Store the credentials - really the refresh token is the most important here as we need it to refresh
									upsertList.push({Name : 'orgId', CurrentValue : creds.orgId, NewValue : null});
									upsertList.push({Name : 'accessToken', CurrentValue : creds.accessToken, NewValue : null});
									upsertList.push({Name : 'userAgent', CurrentValue : creds.userAgent, NewValue : null});
									//upsertList.push({Name : 'userId', CurrentValue : creds.userId, NewValue : null});
									upsertList.push({Name : 'loginUrl', CurrentValue : creds.loginUrl, NewValue : null});
									upsertList.push({Name : 'identityUrl', CurrentValue : creds.identityUrl, NewValue : null});
									upsertList.push({Name : 'communityUrl', CurrentValue : creds.communityUrl, NewValue : null});
									upsertList.push({Name : 'refreshToken', CurrentValue : creds.refreshToken, NewValue : null});
									upsertList.push({Name : 'clientId', CurrentValue : creds.clientId, NewValue : null});
									upsertList.push({Name : 'instanceUrl', CurrentValue : creds.instanceUrl, NewValue : null});
									upsertList.push({Name : 'communityId', CurrentValue : creds.communityId, NewValue : null});

									smartstore.upsertSoupEntries(
											'appSoup',
											upsertList,
											function() {

												// Create soup structure
												var cacheSoupStructure = [
												    {path:"Name",type:"string"},
													{path:"Description",type:"string"}
												];

												// Register the soup
												smartstore.registerSoup(
														'cacheSoup',
														cacheSoupStructure,
										                function() {
															var orientation = getOrientation();

															// Get connection type - default to empty string if not available
															var connType = '';
															if (navigator && navigator.connection) {
																connType = navigator.connection.type;
															}

															// Get mills since the epoch
															var millsSinceEpoch = new Date().getTime();

															// Build up the start url for Salesforce boot page
															var url = 	SupportMc.variables.creds.instanceUrl +
															SupportMc.constants.bootstrapPage +
															'?deviceUuid=' + device.uuid +
															'&deviceName=' + device.name +
															'&deviceCordova=' + device.cordova +
															'&deviceVersion=' + device.version +
															'&deviceModel=' + device.model +
															'&buildName=' + SupportMc.constants.buildName +
															'&buildVersion=' + SupportMc.constants.buildVersion +
															'&buildOS=' + device.platform +
															'&orientation=' + orientation +
															'&viewportWidth=' + $j(window).width() +
															'&viewportHeight=' + $j(window).height() +
															'&sessionType=New Install' + // will revisit when update code done
															'&connType=' + connType +
															'&millsFromEpoch=' + millsSinceEpoch +
															'&userLanguage=' + SupportMc.variables.chosenLanguage;

															// Redirect to start url
															logMessage('Redirecting to: ' + url);
															try {
																window.location.href = url;
															}
															catch (error) {
																logMessage('error on redirect');
																$j('#errorMessage').append('Error:' + error);
																$j.mobile.loading("hide");
															}
														}, // end cachesoup success callback
														error);
											}, // end appsoup success callback
							               	error);
								},
				                error);
					}, // end authenticate callback

					// Display any error from the authenticate call
					function(error) {
						logMessage('authenticate call failed with error = ' + error);

						// Display message on our home page with the error
						$j('#errorMessage').append(error);

						// Show a retry button for the install
						$j("#retryStartupButton").show();
						$j.mobile.loading("hide");
					});
			} // end device online check if
			else {
				$j('#errorMessage').text('Device must be online to install MobileCaddy');

				// Show a retry button for another startup attempt
				$j("#retryStartupButton").show();

				// Display offline message
				$j.mobile.loading("hide");
			} // end device online check else*/

		}, // end startUp

	};
})();
